import React from 'react';
import { TextInput, View, Text } from 'react-native';
import { Controller } from 'react-hook-form';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import styles from '../styles/components/Input';

function Input({ icon, control, errors, name, placeholder, secureTextEntry, disable, testID }) {
  return (
    <Controller
      control={control}
      render={({ field: { onChange, onBlur, value } }) => (
        <View style={styles.container}>
          <View style={styles.form}>
            <Icon name={icon} style={styles.icon} />
            <TextInput
              style={[styles.input, !disable ? styles.active : styles.disabled]}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
              secureTextEntry={secureTextEntry}
              placeholder={placeholder}
              placeholderTextColor={styles.placeholderColor}
              underlineColorAndroid="transparent"
              editable={!disable}
              selectTextOnFocus={!disable}
              testID={testID}
            />
          </View>
          {errors[name]?.message && (
            <Text testID={`${name}_error_validation`} style={styles.error}>
              {errors[name]?.message}
            </Text>
          )}
        </View>
      )}
      name={name}
    />
  );
}

Input.propTypes = {
  icon: PropTypes.string.isRequired,
  control: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  secureTextEntry: PropTypes.bool,
  disable: PropTypes.bool,
  testID: PropTypes.string,
};

Input.defaultProps = {
  placeholder: null,
  secureTextEntry: false,
  disable: false,
  testID: null,
};

export default Input;
