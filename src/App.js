import React, { useEffect } from 'react';
import { LogBox } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from 'react-redux';

import { store } from './redux/store';
import Routes from './routes';

function App() {
  LogBox.ignoreAllLogs();

  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        retry: false,
      },
    },
  });

  useEffect(() => {
    SplashScreen.hide();
  });

  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <SafeAreaProvider>
          <Routes />
        </SafeAreaProvider>
      </Provider>
    </QueryClientProvider>
  );
}

export default App;
