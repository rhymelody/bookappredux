import React from 'react';
import PropTypes from 'prop-types';
import {
  StatusBar,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import { useMutation } from 'react-query';
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { showMessage } from 'react-native-flash-message';
import { yupResolver } from '@hookform/resolvers/yup';

import { postLogin } from '../helpers/api';
import { saveLoginData } from '../redux/action';
import { loginValidation } from '../helpers/form-validation';
import { Input } from '../components';
import bannerIMG from '../assets/images/banner.png';
import styles from '../styles/pages/Login';

function Login({ navigation }) {
  const insets = useSafeAreaInsets();
  const dispatch = useDispatch();

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(loginValidation),
  });

  const { mutate: loginMutation, isLoading } = useMutation(
    async loginData => {
      const response = await postLogin(loginData);
      return response.data;
    },
    {
      throwOnError: true,
      onSuccess: data => {
        if (data.user.isEmailVerified) {
          dispatch(saveLoginData(data));
          navigation.replace('Home');
        } else {
          navigation.replace('Verification');
        }
      },
      onError: error => {
        showMessage({
          message: `[${error.name.toUpperCase()}] ${error.response.data.message}`,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: { textAlign: 'center' },
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    }
  );

  const onSubmit = async data => {
    loginMutation(data);
  };

  return (
    <>
      <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
      <SafeAreaView style={styles.safeArea}>
        <ScrollView contentContainerStyle={styles.container}>
          <Image
            source={bannerIMG}
            resizeMode="contain"
            style={styles.banner}
            testID="banner_image"
          />
          <View style={styles.form}>
            <Input
              name="email"
              placeholder="Email"
              icon="at-sign"
              testID="email_input"
              control={control}
              errors={errors}
              disable={isLoading}
            />
            <Input
              name="password"
              placeholder="Password"
              icon="lock"
              testID="password_input"
              control={control}
              errors={errors}
              disable={isLoading}
              secureTextEntry
            />
            <TouchableOpacity
              testID="login_button"
              style={[
                styles.formButton,
                !isLoading ? styles.formButtonActive : styles.formButtonDisabled,
              ]}
              onPress={handleSubmit(onSubmit)}
              disabled={isLoading}>
              {isLoading && (
                <ActivityIndicator
                  size="small"
                  style={styles.formButton.indicator}
                  color={styles.formButton.indicator.color}
                />
              )}
              <Text style={styles.formButtonText}>Login</Text>
            </TouchableOpacity>
            <View style={styles.navigationContainer}>
              <Text style={styles.navigationText}>Don&apos;t have an account?</Text>
              <TouchableOpacity
                testID="register_link"
                onPress={() => navigation.navigate('Register')}>
                <Text style={styles.navigationLink}>Register</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

Login.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
  }).isRequired,
};

export default Login;
