import 'react-native';
import React from 'react';
import { fireEvent, render, act } from '@testing-library/react-native';

import { Login, Register } from '../pages';

const navigation = { navigate: jest.fn(), replace: jest.fn(), goBack: jest.fn() };

describe('Function & State test', () => {
  describe('Login test', () => {
    it('should input form Login values', async () => {
      const { getByTestId } = render(<Login navigation={navigation} />);

      const { email, password } = {
        email: getByTestId('email_input'),
        password: getByTestId('password_input'),
      };

      await act(async () => {
        fireEvent.changeText(email, 'ilhamsantosa@icloud.com');
        fireEvent.changeText(password, 'ilham123');
      });

      expect(email.props.value).toEqual('ilhamsantosa@icloud.com');
      expect(password.props.value).toEqual('ilham123');
    });

    it('should display all error form validation on Login', async () => {
      const { getByTestId, queryByTestId } = render(<Login navigation={navigation} />);

      await act(async () => {
        fireEvent.press(getByTestId('login_button'));
      });

      expect(queryByTestId('email_error_validation')).toBeTruthy();
      expect(queryByTestId('password_error_validation')).toBeTruthy();
    });

    it('should display email error and password not error form validation on Login', async () => {
      const { getByTestId, queryByTestId } = render(<Login navigation={navigation} />);

      await act(async () => {
        fireEvent.changeText(getByTestId('password_input'), 'ilham123');
        fireEvent.press(getByTestId('login_button'));
      });

      expect(queryByTestId('email_error_validation')).toBeTruthy();
      expect(queryByTestId('password_error_validation')).not.toBeTruthy();
    });

    it('should display email not error and password error form validation on Login', async () => {
      const { getByTestId, queryByTestId } = render(<Login navigation={navigation} />);

      await act(async () => {
        fireEvent.changeText(getByTestId('email_input'), 'ilhamsantosa@icloud.com');
        fireEvent.press(getByTestId('login_button'));
      });

      expect(queryByTestId('email_error_validation')).not.toBeTruthy();
      expect(queryByTestId('password_error_validation')).toBeTruthy();
    });
  });

  describe('Register test', () => {
    it('should input form Register values', async () => {
      const { getByTestId } = render(<Register navigation={navigation} />);
      const { name, email, password } = {
        name: getByTestId('name_input'),
        email: getByTestId('email_input'),
        password: getByTestId('password_input'),
      };
      await act(async () => {
        fireEvent.changeText(name, 'Ahmad Ilham Santosa');
        fireEvent.changeText(email, 'ilhamsantosa@icloud.com');
        fireEvent.changeText(password, 'ilham123');
      });
      expect(name.props.value).toEqual('Ahmad Ilham Santosa');
      expect(email.props.value).toEqual('ilhamsantosa@icloud.com');
      expect(password.props.value).toEqual('ilham123');
    });

    it('should display all error form validation on Register', async () => {
      const { getByTestId, queryByTestId } = render(<Register navigation={navigation} />);
      await act(async () => {
        fireEvent.press(getByTestId('register_button'));
      });
      expect(queryByTestId('name_error_validation')).toBeTruthy();
      expect(queryByTestId('email_error_validation')).toBeTruthy();
      expect(queryByTestId('password_error_validation')).toBeTruthy();
    });

    it('should display name error, email and password not error form validation on Register', async () => {
      const { getByTestId, queryByTestId } = render(<Register navigation={navigation} />);
      await act(async () => {
        fireEvent.changeText(getByTestId('email_input'), 'ilhamsantosa@icloud.com');
        fireEvent.changeText(getByTestId('password_input'), 'ilham123');
        fireEvent.press(getByTestId('register_button'));
      });
      expect(queryByTestId('name_error_validation')).toBeTruthy();
      expect(queryByTestId('email_error_validation')).not.toBeTruthy();
      expect(queryByTestId('password_error_validation')).not.toBeTruthy();
    });

    it('should display email error, name and password not error form validation on Register', async () => {
      const { getByTestId, queryByTestId } = render(<Register navigation={navigation} />);
      await act(async () => {
        fireEvent.changeText(getByTestId('name_input'), 'Ahmad Ilham Santosa');
        fireEvent.changeText(getByTestId('password_input'), 'ilham123');
        fireEvent.press(getByTestId('register_button'));
      });
      expect(queryByTestId('email_error_validation')).toBeTruthy();
      expect(queryByTestId('name_error_validation')).not.toBeTruthy();
      expect(queryByTestId('password_error_validation')).not.toBeTruthy();
    });

    it('should display password error, name and email not error and form validation on Register', async () => {
      const { getByTestId, queryByTestId } = render(<Register navigation={navigation} />);
      await act(async () => {
        fireEvent.changeText(getByTestId('name_input'), 'Ahmad Ilham Santosa');
        fireEvent.changeText(getByTestId('email_input'), 'ilhamsantosa@icloud.com');
        fireEvent.press(getByTestId('register_button'));
      });
      expect(queryByTestId('password_error_validation')).toBeTruthy();
      expect(queryByTestId('name_error_validation')).not.toBeTruthy();
      expect(queryByTestId('email_error_validation')).not.toBeTruthy();
    });
  });
});
