import 'react-native';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';

import { postLogin, postRegister, getBook, getBookList } from '../helpers/api';
import { loginResponse, registerResponse, bookResponse, bookListResponse } from './__dummy__';

describe('Integration test!', () => {
  const mock = new MockAdapter(axios);

  test('Login API', async () => {
    const loginBody = { email: 'ilhamsantosa@icloud.com', password: 'ilham123' };
    mock.onPost('http://code.aldipee.com/api/v1/auth/login').reply(200, loginResponse);

    const api = await postLogin(loginBody);
    expect(api.data).toEqual(loginResponse);
    expect(api.status).toEqual(200);
  });

  test('Register API', async () => {
    const registerBody = {
      email: 'admin@ilham.com',
      password: 'ilham123',
      name: 'Ilham',
    };
    mock.onPost('http://code.aldipee.com/api/v1/auth/register').reply(200, registerResponse);

    const api = await postRegister(registerBody);
    expect(api.data).toEqual(registerResponse);
    expect(api.status).toEqual(200);
  });

  test('Get Book List API', async () => {
    mock.onGet('http://code.aldipee.com/api/v1/books').reply(200, bookListResponse);

    const api = await getBookList();
    expect(api.data).toEqual(bookListResponse);
    expect(api.status).toEqual(200);
  });

  test('Get Book API', async () => {
    mock
      .onGet('http://code.aldipee.com/api/v1/books/6231453513c01e6f8b566ece')
      .reply(200, bookResponse);

    const api = await getBook('6231453513c01e6f8b566ece');
    expect(api.data).toEqual(bookResponse);
    expect(api.status).toEqual(200);
  });
});
