import loginResponse from './login-response';
import registerResponse from './register-response';
import bookListResponse from './book-list-response';
import bookResponse from './book-response';

export { loginResponse, registerResponse, bookListResponse, bookResponse };
