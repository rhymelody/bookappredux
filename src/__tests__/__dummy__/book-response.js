const bookResponse = {
  title: 'Harry Potter and the Order of the Phoenix',
  author: 'J. K. Rowling',
  cover_image:
    'https://images-na.ssl-images-amazon.com/images/I/51SfTd37PaL._SX415_BO1,204,203,200_.jpg',
  id: '6231453513c01e6f8b566ece',
  page_count: '896',
  publisher: 'Scholastic',
  synopsis:
    "In his fifth year at Hogwart's, Harry faces challenges at every turn, from the dark threat of He-Who-Must-Not-Be-Named and the unreliability of the government of the magical world to the rise of Ron Weasley as the keeper of the Gryffindor Quidditch Team. Along the way he learns about the strength of his friends, the fierceness of his enemies, and the meaning of sacrifice",
  total_sale: '644',
  average_rating: '8',
  price: '292532',
  stock_available: '51',
};

export default bookResponse;
