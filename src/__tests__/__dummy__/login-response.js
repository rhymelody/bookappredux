const loginResponse = {
  user: {
    role: 'user',
    isEmailVerified: true,
    email: 'ilhamsantosa@icloud.com',
    name: 'Ilham Santosa',
    id: '62513abf22536c7fc70026e8',
  },
  tokens: {
    access: {
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjUxM2FiZjIyNTM2YzdmYzcwMDI2ZTgiLCJpYXQiOjE2NTA0NzIwMzEsImV4cCI6MTY1MDQ3MzgzMSwidHlwZSI6ImFjY2VzcyJ9.kjR19_pNcbVfaEgB2ZUeCEcp5bpBlZlny4diVkJXn84',
      expires: '2022-04-20T16:57:11.872Z',
    },
    refresh: {
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjUxM2FiZjIyNTM2YzdmYzcwMDI2ZTgiLCJpYXQiOjE2NTA0NzIwMzEsImV4cCI6MTY1MzA2NDAzMSwidHlwZSI6InJlZnJlc2gifQ.pC7z5lxqmg2_rFgpyw8f-g46lfxRqcdBxECgchKNkXE',
      expires: '2022-05-20T16:27:11.873Z',
    },
  },
};

export default loginResponse;
