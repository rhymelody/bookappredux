const registerResponse = {
  success: true,
  message: 'Register succeed! Please try to login with email admin@ilham.com',
  data: {
    role: 'user',
    isEmailVerified: true,
    email: 'admin@ilham.com',
    name: 'Ilham',
    id: '6261897a5c6ac22fcb8028c3',
  },
};

export default registerResponse;
