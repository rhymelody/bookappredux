import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';

import { Login, Register, HomeScreen } from '../pages';

const navigation = { navigate: jest.fn(), replace: jest.fn(), goBack: jest.fn() };

describe('Snapshot test', () => {
  it('should render Login Component correctly', () => {
    const component = shallow(<Login navigation={navigation} />);

    expect(component).toMatchSnapshot();
  });

  it('should render Register Component correctly', () => {
    const component = shallow(<Register navigation={navigation} />);

    expect(component).toMatchSnapshot();
  });

  it('should render HomeScreen Component correctly', () => {
    const component = shallow(<HomeScreen navigation={navigation} />);

    expect(component).toMatchSnapshot();
  });
});
