import React, { useEffect, useRef } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NetInfo from '@react-native-community/netinfo';
import FlashMessage, { showMessage } from 'react-native-flash-message';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import EStyleSheet from './styles';
import { HomeScreen, BookDetail, Login, Register, Verification } from './pages';

const Stack = createNativeStackNavigator();

function Routes() {
  const insets = useSafeAreaInsets();
  const isInitialMount = useRef(true);

  useEffect(() => {
    NetInfo.addEventListener((state) => {
      if (state.isConnected) {
        !isInitialMount.current &&
          showMessage({
            message: 'Internet Connected',
            type: 'success',
            backgroundColor: EStyleSheet.value('$base.success'),
            titleStyle: { textAlign: 'center' },
            statusBarHeight: insets.top,
          });
      } else {
        showMessage({
          message: 'No Internet Connection',
          type: 'danger',
          backgroundColor: EStyleSheet.value('$base.danger'),
          titleStyle: { textAlign: 'center' },
          statusBarHeight: insets.top,
        });
      }
    });

    setTimeout(() => {
      if (isInitialMount.current) {
        isInitialMount.current = false;
      }
    }, 5000);
  });

  return (
    <>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Verification" component={Verification} />
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen
            name="Book Detail"
            component={BookDetail}
            initialParams={{ id: '6231453513c01e6f8b566ece' }}
          />
        </Stack.Navigator>
      </NavigationContainer>
      <FlashMessage />
    </>
  );
}

export default Routes;
