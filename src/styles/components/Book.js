import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    recommended: {
      width: '7.25rem',
      marginHorizontal: '.5rem',
    },
    popular: {
      width: '33% - 1.92rem',
      margin: '.5rem',
    },
  },
  contentImage: {
    position: 'relative',
    image: {
      recommended: {
        height: '10.5rem',
        width: '100%',
        borderRadius: '.5rem',
      },
      popular: {
        height: '9rem',
        width: '100%',
        borderRadius: '.5rem',
      },
    },
    rating: {
      position: 'absolute',
      zIndex: 1,
      right: 0,
      backgroundColor: '#000000bf',
      padding: '.25rem',
      width: '2.275rem',
      borderTopRightRadius: '.5rem',
      borderBottomLeftRadius: '.5rem',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      icon: {
        color: '$base.rating',
        fontSize: '.675rem',
      },
      text: {
        fontSize: '.675rem',
        color: '$base.rating',
        marginLeft: '.275rem',
      },
    },
  },
  contentDetails: {
    paddingVertical: '.375rem',
    title: {
      color: '$netral[500]',
      fontSize: '.875rem',
      fontWeight: 'bold',
      marginBottom: '.125rem',
    },
    subtitle: {
      color: '$secondary[500]',
      fontSize: '.75rem',
      marginBottom: '.375rem',
    },
    row: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    price: {
      color: '$secondary[500]',
      fontWeight: 'bold',
      fontSize: '.825rem',
    },
    rating: {
      flexDirection: 'row',
      alignItems: 'center',
      icon: {
        color: '#f1c40e',
        fontSize: '.775rem',
      },
      text: {
        fontSize: '.775rem',
        color: '#f1c40e',
        marginLeft: '.275rem',
      },
    },
  },
  shimmer: {
    title: {
      width: '100%',
      marginBottom: '.375rem',
      color: ['#8f959d', '#666666', '#8f959d'],
      borderRadius: '.25rem',
    },
    subtitle: {
      width: '100%',
      marginBottom: '.375rem',
      borderRadius: '.25rem',
    },
    price: {
      recommended: {
        width: '50%',
        marginRight: '1rem',
        marginTop: '.25rem',
        marginBottom: '.5rem',
        borderRadius: '.25rem',
      },
      popular: {
        width: '100%',
        marginRight: '1rem',
        marginTop: '.25rem',
        marginBottom: '.5rem',
        borderRadius: '.25rem',
      },
    },
    rating: {
      width: '2.5rem',
      marginTop: '.25rem',
      marginBottom: '.5rem',
      color: ['#dea36c', '#ebebeb', '#dea36c'],
      borderRadius: '.25rem',
    },
  },
});

export default styles;
