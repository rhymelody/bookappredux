import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  safeArea: {
    backgroundColor: '$background',
    flexGrow: 1,
    paddingBottom: '6.375rem',
  },
  header: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '$primary[500]',
    padding: '1.5rem',
  },
  headerGreeting: {
    color: '$base.white',
    fontSize: '1rem',
    fontWeight: '400',
  },
  headerName: {
    color: '$base.white',
    fontSize: '1rem',
    fontWeight: '500',
  },
  headerLogOut: {
    fontSize: '1.5rem',
    color: '$base.white',
  },
  container: {
    backgroundColor: '$background',
    padding: '1.5rem',
    width: '100%',
    flexGrow: 1,
  },
  recomendation: {
    marginBottom: '2rem',
  },
  recomendationHeader: {
    color: '$netral[500]',
    fontSize: '1.125rem',
    fontWeight: 'bold',
    marginBottom: '1rem',
  },
  recomendationContent: {
    flexDirection: 'row',
    marginHorizontal: '-.5rem',
  },
  popularHeader: {
    color: '$netral[500]',
    fontSize: '1.125rem',
    fontWeight: 'bold',
    marginBottom: '1rem',
  },
  popularContent: {
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'wrap',
    margin: '-.5rem',
  },
});

export default styles;
