import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  $headerMinHeight: '5rem',
  $headerMaxHeight: '12rem',
  safeArea: {
    backgroundColor: '$background',
    height: '100%',
    flexGrow: 1,
  },
  notification: {
    backgroundColor: '$primary[500]',
  },
  header: {
    minHeight: '$headerMinHeight',
    maxHeight: '$headerMaxHeight + 4.75rem',
    height: '$headerMaxHeight + 4.75rem',
    backgroundColor: '$background',
    tint: {
      backgroundColor: '$primary[500]',
      borderBottomLeftRadius: '.5rem',
      borderBottomRightRadius: '.5rem',
      image: {
        height: '$headerMaxHeight',
        width: '100%',
        borderBottomLeftRadius: '.5rem',
        borderBottomRightRadius: '.5rem',
      },
    },
    sticky: {
      backgroundColor: '$primary[500]',
    },
    navigation: {
      position: 'absolute',
      width: '100%',
      height: '5rem',
      paddingHorizontal: '1.5rem',
      flexDirection: 'row',
      action: {
        button: {
          backgroundColor: 'rgba(27, 39, 75, 0.5)',
          height: '2.25rem',
          width: '2.25rem',
          borderRadius: '2.25rem / 2',
          alignItems: 'center',
          justifyContent: 'center',
        },
        icon: {
          color: '$base.white',
          fontSize: '1.275rem',
        },
      },
      actionLeft: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
      },
      actionRight: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
      },
    },
    book: {
      flex: 1,
      flexGrow: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
      paddingHorizontal: '1.5rem',
      content: {
        backgroundColor: '$secondary[100]',
        borderRadius: '1rem',
        width: '100%',
        height: '11.5rem',
        paddingVertical: '1.125rem',
        paddingHorizontal: '1rem',
        flexDirection: 'row',
        contentImage: {
          justifyContent: 'center',
          alignItems: 'center',
          marginRight: '.5rem',
          image: {
            height: '9.25rem',
            width: '6rem',
            borderRadius: '.5rem',
          },
        },
        contentDetails: {
          flexShrink: 1,
          paddingVertical: '1rem',
          paddingLeft: '.5rem',
          paddingRight: '1rem',
          justifyContent: 'center',
          title: {
            flexGrow: 1,
            color: '$netral[900]',
            fontSize: '1.25rem',
            fontWeight: '900',
            marginBottom: '.75rem',
          },
          author: {
            flexGrow: 1,
            color: '$netral[500]',
            fontSize: '.875rem',
            marginBottom: '.25rem',
            fontWeight: '500',
          },
          publisher: {
            flexGrow: 1,
            color: '$netral[500]',
            fontSize: '.875rem',
            marginBottom: '.75rem',
            fontWeight: '500',
          },
        },
      },
    },
  },
  container: {
    backgroundColor: '$background',
    paddingVertical: '2.25rem',
    paddingHorizontal: '2.25rem',
    width: '100%',
    minHeight: '100%',
    flexGrow: 1,
  },
  content: {
    action: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      backgroundColor: '$secondary[100]',
      height: '4.25rem',
      marginTop: '-1rem',
      marginHorizontal: '-.5rem',
      marginBottom: '2rem',
      paddingLeft: '.5rem',
      borderRadius: '.5rem',
      rating: {
        alignItems: 'center',
        justifyContent: 'center',
        header: {
          color: '$netral[500]',
          fontWeight: '700',
          marginBottom: '.25rem',
        },
        content: {
          flexDirection: 'row',
          alignItems: 'center',
          icon: {
            color: '$base.rating',
          },
          text: {
            color: '$base.rating',
            fontWeight: '500',
            marginLeft: '.275rem',
          },
        },
      },
      sale: {
        alignItems: 'center',
        justifyContent: 'center',
        header: {
          color: '$netral[900]',
          fontWeight: '700',
          marginBottom: '.25rem',
        },
        content: {
          color: '$netral[500]',
          fontWeight: '500',
        },
      },
      buy: {
        alignItems: 'center',
        justifyContent: 'center',
        button: {
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '$primary[500]',
          paddingVertical: '.625rem',
          paddingHorizontal: '1.125rem',
          borderRadius: '.5rem',
          height: '2.575rem',
          minWidth: '8.75rem',
          text: {
            color: '$base.white',
          },
        },
      },
    },
    overview: {
      marginBottom: '2rem',
      header: {
        color: '$netral[700]',
        fontSize: '1.125rem',
        fontWeight: 'bold',
        marginBottom: '.75rem',
      },
      content: {
        color: '$netral[500]',
        fontSize: '.875rem',
      },
    },
  },
  shimmer: {
    title: {
      width: '10rem',
      marginBottom: '.75rem',
      color: ['#8f959d', '#666666', '#8f959d'],
      borderRadius: '.25rem',
    },
    author: {
      width: '6.75rem',
      marginBottom: '.25rem',
      borderRadius: '.25rem',
    },
    publisher: {
      width: '5rem',
      marginRight: '1rem',
      marginBottom: '.75rem',
      borderRadius: '.25rem',
    },
    rating: {
      width: '2.5rem',
      height: '1.25rem',
      color: ['#dea36c', '#ebebeb', '#dea36c'],
      borderRadius: '.25rem',
    },
    sale: {
      width: '3.5rem',
      height: '1.25rem',
      borderRadius: '.25rem',
    },
    button: {
      height: '2.575rem',
      width: '8.75rem',
      color: ['#1B274B', '#4C5F93', '#1B274B'],
      borderRadius: '.5rem',
    },
    synopsis: {
      marginBottom: '.3rem',
      borderRadius: '.25rem',
    },
  },
});

export default styles;
