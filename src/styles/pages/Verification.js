import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  safeArea: {
    backgroundColor: '$background',
    flexGrow: 1,
  },
  container: {
    backgroundColor: '$background',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: '1.5rem',
    paddingVertical: '3.5rem',
    width: '100%',
    flexGrow: 1,
  },
  header: {
    text: {
      marginTop: '.675rem',
      color: '$netral[900]',
      fontSize: '1.5rem',
      marginBottom: '.5rem',
      fontWeight: '700',
      textAlign: 'center',
    },
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    iconContainer: {
      backgroundColor: '$primary[300]',
      alignItems: 'center',
      justifyContent: 'center',
      width: '7.5rem',
      height: '7.5rem',
      borderRadius: '7.5rem / 2',
      marginBottom: '1.5rem',
      icon: {
        color: '$base.white',
        fontSize: '3.75rem',
      },
    },
    text: {
      fontSize: '1rem',
      fontWeight: '500',
    },
  },
  footer: {
    width: '100%',
    button: {
      backgroundColor: '$primary[500]',
      paddingVertical: '.75rem',
      paddingHorizontal: '1rem',
      borderRadius: '.5rem',
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      text: {
        color: '$base.white',
        fontSize: '1.125rem',
        fontWeight: '600',
      },
    },
  },
});

export default styles;
