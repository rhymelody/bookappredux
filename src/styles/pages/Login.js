import EStyleSheet from 'react-native-extended-stylesheet';
import { Dimensions } from 'react-native';

const screen = Dimensions.get('screen');

const styles = EStyleSheet.create({
  safeArea: {
    backgroundColor: '$background',
    flexGrow: 1,
  },
  container: {
    backgroundColor: '$background',
    padding: '1.5rem',
    width: '100%',
    flexGrow: 1,
  },
  banner: {
    width: '100%',
    height: `${screen.height} / 4.275`,
    marginBottom: '2.5rem',
  },
  form: {
    header: {
      color: '$netral[900]',
      fontSize: '1.5rem',
      fontWeight: '900',
      marginBottom: '1.5rem',
      textAlign: 'center',
    },
  },
  formButton: {
    marginTop: '1.25rem',
    marginBottom: '1.25rem',
    paddingVertical: '.75rem',
    paddingHorizontal: '1rem',
    borderRadius: '.5rem',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '$primary[500]',
    indicator: {
      color: '$base.white',
      marginRight: '1rem',
    },
  },
  formButtonActive: {
    opacity: 1,
  },
  formButtonDisabled: {
    opacity: 0.2,
  },
  formButtonText: {
    color: '$base.white',
    fontSize: '1rem',
    fontWeight: '600',
  },
  navigationContainer: {
    alignItems: 'center',
  },
  navigationText: {
    fontSize: '.875rem',
    fontWeight: '500',
    color: '$secondary[500]',
    marginBottom: '.25rem',
  },
  navigationLink: {
    fontSize: '.875rem',
    fontWeight: '800',
    color: '$netral[500]',
  },
});

export default styles;
