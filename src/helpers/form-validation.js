import * as yup from 'yup';
import YupPassword from 'yup-password';

YupPassword(yup);

export const loginValidation = yup
  .object({
    email: yup.string().email('Invalid email address.').required('Email is required.'),
    password: yup.string().required('Password is required.'),
  })
  .required();

export const registerValidation = yup
  .object({
    name: yup.string().required('Name required.'),
    email: yup.string().email('Invalid email address.').required('Email is required.'),
    password: yup
      .string()
      .required('Password is required.')
      .min(8, 'Password must be 8 characters long.')
      .matches(
        /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i,
        'Password must contain at least 1 letter and 1 number.'
      ),
  })
  .required();
