import { store } from '../redux/store';

export const getToken = () => {
  return store.getState().data && store.getState().data.tokens.access.token;
};

export const getName = () => {
  return store.getState().data && store.getState().data.user.name;
};

export const getVerifiedStatus = () => {
  return store.getState().data && store.getState().data.user.isEmailVerified;
};
