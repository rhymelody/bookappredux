import axios from 'axios';
import { API_ENDPOINT } from '../../config';
import { getToken } from '../user-data';

const getBookList = async () => {
  const response = await axios
    .get(`${API_ENDPOINT}/books`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
    .catch((error) => {
      throw error;
    });

  return response;
};

export default getBookList;
