import axios from 'axios';
import { API_ENDPOINT } from '../../config';

const postRegister = async (data) => {
  const response = await axios.post(`${API_ENDPOINT}/auth/register`, data).catch((error) => {
    throw error;
  });

  return response;
};

export default postRegister;
