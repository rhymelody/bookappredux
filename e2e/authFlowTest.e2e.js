describe('Authentication flow test', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  it('should tap register link', async () => {
    await element(by.id('register_link')).tap();
  });

  it('should tap register button', async () => {
    await element(by.id('register_button')).tap();
  });

  it('should fill register form', async () => {
    await element(by.id('name_input')).typeText('Ahmad Ilham Santosa');
    await element(by.id('email_input')).typeText('ilhamsantosa@detox2.com');
    await element(by.id('password_input')).typeText('ilham123\n');
  });

  it('should tap register button', async () => {
    await element(by.id('register_button')).tap();
  });

  it('should wait 3 sec and tap back to login button', async () => {
    await new Promise(resolve => setTimeout(resolve, 3000));
    await element(by.id('back_to_login_button')).tap();
  });

  it('should fill login form', async () => {
    await element(by.id('email_input')).typeText('ilhamsantosa@detox2.com');
    await element(by.id('password_input')).typeText('ilham123\n');
  });

  it('should tap login button and redirect to home screen (5 sec)', async () => {
    await element(by.id('login_button')).tap();
    await expect(element(by.id('greeting_text'))).toBeVisible();
    await new Promise(resolve => setTimeout(resolve, 5000));
  });
});
