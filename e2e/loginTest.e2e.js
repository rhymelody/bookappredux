describe('Login test', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  it('should have banner image', async () => {
    await expect(element(by.id('banner_image'))).toBeVisible();
  });

  it('should tap login button', async () => {
    await element(by.id('login_button')).tap();
  });

  it('should tap register link and back to login', async () => {
    await element(by.id('register_link')).tap();
    await element(by.id('back_to_login_button')).tap();
  });

  it('should fill login form', async () => {
    await element(by.id('email_input')).typeText('ilhamsantosa@icloud.com');
    await element(by.id('password_input')).typeText('ilham123');
  });
});
