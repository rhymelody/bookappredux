describe('Register test', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  it('should tap register link and back to login', async () => {
    await element(by.id('register_link')).tap();
    await element(by.id('back_to_login_button')).tap();
  });

  it('should tap back to register link and tap login', async () => {
    await element(by.id('register_link')).tap();
    await element(by.id('login_link')).tap();
  });

  it('should tap back to register link', async () => {
    await element(by.id('register_link')).tap();
  });

  it('should tap register button', async () => {
    await element(by.id('register_button')).tap();
  });

  it('should fill register form', async () => {
    await element(by.id('name_input')).typeText('Ahmad Ilham Santosa');
    await element(by.id('email_input')).typeText('ilhamsantosa@icloud.com');
    await element(by.id('password_input')).typeText('ilham123');
  });
});
