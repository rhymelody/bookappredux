/* eslint-disable no-undef */
import * as ReactNative from 'react-native';
import mockRNCNetInfo from '@react-native-community/netinfo/jest/netinfo-mock';
import * as redux from 'react-redux';
import * as reactQuery from 'react-query';

jest.doMock('react-native', () => {
  return Object.setPrototypeOf(
    {
      StyleSheet: {
        create: jest.fn(),
      },
      Platform: {
        OS: 'android',
        select: () => {},
      },
      NativeModules: {
        ...ReactNative.NativeModules,
        NotifeeApiModule: {
          addListener: jest.fn(),
          eventsAddListener: jest.fn(),
          eventsNotifyReady: jest.fn(),
        },
      },
    },
    ReactNative
  );
});

jest.mock('react-native-sound', () => {
  class SoundMock {}

  SoundMock.prototype.setVolume = jest.fn();
  SoundMock.prototype.setNumberOfLoops = jest.fn();
  SoundMock.prototype.play = jest.fn();
  SoundMock.prototype.stop = jest.fn();

  SoundMock.setCategory = jest.fn();

  return SoundMock;
});

jest.mock('@react-native-community/netinfo', () => mockRNCNetInfo);
jest.mock('react-native-safe-area-context', () => {
  const inset = { top: 0, right: 0, bottom: 0, left: 0 };
  return {
    ...jest.requireActual('react-native-safe-area-context'),
    SafeAreaProvider: jest.fn(({ children }) => children),
    SafeAreaConsumer: jest.fn(({ children }) => children(inset)),
    useSafeAreaInsets: jest.fn(() => inset),
    useSafeAreaFrame: jest.fn(() => ({ x: 0, y: 0, width: 390, height: 844 })),
  };
});

const useDispatchSpy = jest.spyOn(redux, 'useDispatch');
useDispatchSpy.mockReturnValue(jest.fn());

const useMutationSpy = jest.spyOn(reactQuery, 'useMutation');
useMutationSpy.mockReturnValue(jest.fn());

const useQuerySpy = jest.spyOn(reactQuery, 'useQuery');
useQuerySpy.mockReturnValue(jest.fn());
